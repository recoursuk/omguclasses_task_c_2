//
// Created by asbox on 10.04.2020.
//

#ifndef TASK_2_QUEUE_H
#define TASK_2_QUEUE_H


class Queue {
    int front{}, tail{};
    int size{};
    int **data;

public:
    Queue(int size);

    ~Queue();

    void addElem(int elem);

    int get();

    int pick();

    int getSize();

    void clear();

    bool isEmpty();

    void printQueue();

    friend class Iterator;
};


#endif //TASK_2_QUEUE_H
