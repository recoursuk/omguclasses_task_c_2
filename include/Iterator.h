//
// Created by asbox on 10.04.2020.
//

#ifndef TASK_2_ITERATOR_H
#define TASK_2_ITERATOR_H


#include "Queue.h"

class Iterator {
    Queue *queue;
    int current;
public:
    Iterator(Queue &queue);

    void start();

    void next();

    bool finish();

    int getValue();
};


#endif //TASK_2_ITERATOR_H
