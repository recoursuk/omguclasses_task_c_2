//
// Created by asbox on 10.04.2020.
//

#include <stdexcept>
#include "../include/Iterator.h"

Iterator::Iterator(Queue &queue) {
    this->queue = &queue;
    this->current = -1;
}

void Iterator::start() {
    current = queue->front;
}

void Iterator::next() {
    if(current != -1){
        if(current == queue->size - 1){
            current = 0;
        } else{
            current++;
        }
    }
}

bool Iterator::finish() {
    return current - 1 == queue->tail || (current == 0 && queue->tail == queue->size - 1);
}

int Iterator::getValue() {
    if (current != -1) {
        int temp = *queue->data[current];
        return temp;
    }
    throw std::logic_error("Start first");
}