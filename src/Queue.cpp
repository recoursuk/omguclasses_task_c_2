//
// Created by asbox on 10.04.2020.
//

#include <stdexcept>
#include <iostream>
#include "../include/Queue.h"

Queue::Queue(int size) {
    this->front = -1;
    this->tail = -1;
    this->size = size;
    this->data = new int*[size];
    for(int i = 0; i < size; i++){
        data[i] = nullptr;
    }
}

Queue::~Queue() {
    delete [] data;
}

void Queue::addElem(int elem) {
    if ((front == 0 && tail == size-1) ||
        (tail == (front-1)%(size-1)))
    {
        throw std::logic_error("Queue is full");
    }

    else if (front == -1) /* Insert First Element */
    {
        front = tail = 0;
        data[tail] = new int(elem);
    }

    else if (tail == size-1 && front != 0)
    {
        tail = 0;
        data[tail] = new int(elem);
    }

    else
    {
        tail++;
        data[tail] = new int(elem);
    }
}

int Queue::get() {
    if (front == -1)
    {
        throw std::logic_error("Queue is empty");
    }

    int temp = *data[front];
    data[front] = nullptr;
    if (front == tail)
    {
        front = -1;
        tail = -1;
    }
    else if (front == size-1)
        front = 0;
    else
        front++;

    return temp;
}

int Queue::pick() {
    if (front == -1)
    {
        throw std::logic_error("Queue is empty");
    }

    int temp = *data[front];
    return temp;
}

int Queue::getSize() {
    return size;
}

void Queue::clear() {
    for(int i = 0; i < size; i++){
        data[i] = nullptr;
    }
    front = -1;
    tail = -1;
}

bool Queue::isEmpty() {
    return front == -1;
}

void Queue::printQueue() {
    if (front == -1)
    {
        std::cout << "\nQueue is Empty";
        return;
    }
    std::cout << "\nElements in Circular Queue are: ";
    if (tail >= front)
    {
        for (int i = front; i <= tail; i++)
            std::cout <<  *data[i];
    }
    else
    {
        for (int i = front; i < size; i++)
            std::cout << *data[i];

        for (int i = 0; i <= tail; i++)
            std::cout << *data[i];
    }
}
