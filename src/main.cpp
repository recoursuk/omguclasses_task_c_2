#include <iostream>

#include "../include/queue.h"
#include "../include/iterator.h"


using namespace std;

int main() {
    Queue q(5);
    Iterator iterator(q);

    q.addElem(29);
    q.addElem(8);
    q.addElem(33);
    q.addElem(-5);

    iterator.start();
    while (!iterator.finish()){
        cout << iterator.getValue() << " ";
        iterator.next();
    }

    cout << endl;

    cout << q.get() << endl;
    cout << q.get() << endl;

    iterator.start();
    while (!iterator.finish()){
        cout << iterator.getValue() << " ";
        iterator.next();
    }

    return 0;
}

